module Main where

import Lib
import System.Environment
import System.Exit
import Servant.Client
import Network.HTTP.Client (newManager, defaultManagerSettings)

run :: String -> String -> IO ()
run key zc = do
    manager <- newManager defaultManagerSettings
    res <- runClientM (apiConditions zc) (ClientEnv manager (BaseUrl Http "api.wunderground.com" 80 ("/api/" ++ key)))
    case res of
        Left err -> putStrLn $ "Error: " ++ show err
        Right conds -> print conds

main :: IO ()
main = getEnv "WU_API_KEY" >>= 
    (\apiKey -> getArgs >>=
    parse >>=
    addFormat >>=
    mapM_ (run apiKey))

parse :: [String] -> IO [String]
parse ["-h"] = usage   >> exitSuccess
parse ["-v"] = version >> exitSuccess
parse []     = usage >> exitSuccess
parse xs     = return xs

usage :: IO ()
usage   = putStrLn "Usage: wu [-vh] [zip ..]"

version :: IO ()
version = putStrLn "Haskell wu 0.1"

addFormat :: [String] -> IO [String]
addFormat = mapM (return . (++ ".json"))
