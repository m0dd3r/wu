{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Lib
    ( apiConditions
    , ApiResponse
    ) where

import Data.Aeson
import Data.Aeson.Casing
import Data.Proxy
import GHC.Generics
import Servant.API
import Servant.Client

data CurrentObservation = CurrentObservation {
        obsTempF :: Float
        , obsRelativeHumidity :: String
        , obsWeather :: String} deriving (Show, Generic)

instance FromJSON CurrentObservation where
        parseJSON = genericParseJSON $ aesonPrefix snakeCase

data ApiResponse = ApiResponse {
        rResponse :: Response
        , rCurrentObservation :: CurrentObservation} deriving (Show, Generic)

instance FromJSON ApiResponse where
        parseJSON = genericParseJSON $ aesonPrefix snakeCase

data Response = Response {
        respVersion :: String
        , respFeatures :: Feature} deriving (Show, Generic)

instance FromJSON Response where
        parseJSON = genericParseJSON $ aesonPrefix snakeCase

newtype Feature = Feature {
        featConditions :: Int} deriving (Show, Generic)

instance FromJSON Feature where
        parseJSON = genericParseJSON $ aesonPrefix snakeCase



type API  = "conditions" :> "q" :> Capture "zip" String :> Get '[JSON] ApiResponse

api :: Proxy API
api = Proxy

apiConditions :: String -> ClientM ApiResponse
apiConditions = client api
